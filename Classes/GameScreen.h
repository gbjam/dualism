//
//  GameScreen.h
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/2/14.
//
//

#ifndef __Dualism__GameScreen__
#define __Dualism__GameScreen__

#include <iostream>
#include "cocos2d.h"
#include "Player.h"
#include "World.h"
#include "MGCamera.h"

USING_NS_CC;

class GameLayer;
class GameScreen :
public
    Scene
{
public:
    GameScreen();
    virtual ~GameScreen();
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
private:
    GameLayer* m_gameLayer;
    Player* m_player;
    
public:
    // implement the "static create()" method manually
    CREATE_FUNC(GameScreen);
};

#endif /* defined(__Dualism__GameScreen__) */
