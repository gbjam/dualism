//
//  MGSingleton.h
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/4/14.
//
//

#ifndef __Dualism__MGSingleton__
#define __Dualism__MGSingleton__

#include <iostream>

#define MG_ASSERT(INSTANCE)

#define DECLARE_SINGLETON(CLASS_NAME) \
    public: \
    inline static CLASS_NAME& Instance() { MG_ASSERT(m_pInstance); return *m_pInstance; } \
    static void Create() { if( !m_pInstance ) m_pInstance = new CLASS_NAME; } \
    static void Delete() { if( m_pInstance ) delete m_pInstance; m_pInstance = 0; } \
    static bool Created() { return m_pInstance != 0; } \
    protected: \
    CLASS_NAME(); \
    private: \
    CLASS_NAME(const CLASS_NAME& p_rOther); \
    CLASS_NAME& operator=(const CLASS_NAME &p_rOther ); \
    static CLASS_NAME* m_pInstance;

#define DECLARE_SINGLETON_NO_AUTO_CREATE_METHOD(CLASS_NAME) \
    public: \
    inline static CLASS_NAME& Instance() { MG_ASSERT(m_pInstance); return *m_pInstance; } \
    static void Create(); \
    static void Delete() { if( m_pInstance ) delete m_pInstance; m_pInstance = 0; } \
    static bool Created() { return m_pInstance != 0; } \
    protected: \
    CLASS_NAME(); \
    private: \
    CLASS_NAME(const CLASS_NAME& p_rOther); \
    CLASS_NAME& operator=(const CLASS_NAME &p_rOther ); \
    static CLASS_NAME* m_pInstance;

#define DECLARE_AUTOMATIC_SINGLETON(CLASS_NAME) \
    public: \
    inline static CLASS_NAME& Instance() { if( !m_pInstance) Create(); return *m_pInstance; } \
    static void Create() { if( !m_pInstance ) m_pInstance = new CLASS_NAME; } \
    static void Delete() { if( m_pInstance ) delete m_pInstance; m_pInstance = 0; } \
    static bool Created() { return m_pInstance != 0; } \
    private: \
    CLASS_NAME(); \
    CLASS_NAME(const CLASS_NAME& p_rOther); \
    CLASS_NAME& operator=(const CLASS_NAME &p_rOther ); \
    static CLASS_NAME* m_pInstance;


#define DEFINE_SINGLETON(CLASS_NAME)	CLASS_NAME* CLASS_NAME::m_pInstance = 0;

#endif /* defined(__Dualism__MGSingleton__) */
