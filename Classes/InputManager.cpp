//
//  InputManager.cpp
//  Dualism
//
//  Created by Charles Alvarez on 8/4/14.
//
//

#include "InputManager.h"

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//INITIALIZATION
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static InputManager*                                        g_InputManager = nullptr;
static std::vector<InputReciever*>                          g_reciever;
static std::map<EventKeyboard::KeyCode, Enum::ControlKeys>  g_mapKeys =
{
    {EventKeyboard::KeyCode::KEY_A,             Enum::KEY_A},
    {EventKeyboard::KeyCode::KEY_CAPITAL_A,     Enum::KEY_A},
    {EventKeyboard::KeyCode::KEY_S,             Enum::KEY_S},
    {EventKeyboard::KeyCode::KEY_CAPITAL_S,     Enum::KEY_S},
    {EventKeyboard::KeyCode::KEY_D,             Enum::KEY_D},
    {EventKeyboard::KeyCode::KEY_CAPITAL_D,     Enum::KEY_D},
    {EventKeyboard::KeyCode::KEY_W,             Enum::KEY_W},
    {EventKeyboard::KeyCode::KEY_CAPITAL_W,     Enum::KEY_W},
    {EventKeyboard::KeyCode::KEY_J,             Enum::KEY_J},
    {EventKeyboard::KeyCode::KEY_CAPITAL_J,     Enum::KEY_J},
    {EventKeyboard::KeyCode::KEY_K,             Enum::KEY_K},
    {EventKeyboard::KeyCode::KEY_CAPITAL_K,     Enum::KEY_K},
    {EventKeyboard::KeyCode::KEY_B,             Enum::KEY_SELECT},
    {EventKeyboard::KeyCode::KEY_CAPITAL_B,     Enum::KEY_SELECT},
    {EventKeyboard::KeyCode::KEY_N,             Enum::KEY_START},
    {EventKeyboard::KeyCode::KEY_CAPITAL_N,     Enum::KEY_START},
};

InputManager* InputManager::GetInstance()
{
    if(!g_InputManager)
    {
        g_InputManager = new InputManager();
    }
    
    return g_InputManager;
}

InputManager::InputManager()
{
    //~~~Flags for keyboard
    //   To know if there are 2 or more keyboard pressed at the same time ex. A + B
    m_keyboardFlags         = 0x0;
    
    //~~~Initialize listener and add it to even dispatcher
    auto listener           = EventListenerKeyboard::create();
    listener->onKeyPressed  = CC_CALLBACK_2(InputManager::KeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(InputManager::KeyReleased, this);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 1);
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//HANDLING OF DELEGATES
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void InputManager::KeyPressed(EventKeyboard::KeyCode p_code, Event* p_event)
{
    if (g_mapKeys.find( p_code ) != g_mapKeys.end())
    {
        //~~~Access pressed key and add it to the keybored flags
        Enum::ControlKeys code  = g_mapKeys[p_code];
        m_keyboardFlags         |= code;
        
        //~~~Call all Delegates
        for ( std::vector<InputReciever*>::iterator it = g_reciever.begin(); it != g_reciever.end(); it++ )
        {
            ((InputReciever*)*it)->KeyPressed(m_keyboardFlags, code);
        }
    }
}

//~~~Invoker : keyboard released
void InputManager::KeyReleased(EventKeyboard::KeyCode p_code, Event* p_event)
{
    if (g_mapKeys.find( p_code ) != g_mapKeys.end())
    {
        //~~~Access pressed key and remove it to the keybored flags
        Enum::ControlKeys code  = g_mapKeys[p_code];
        m_keyboardFlags         &= (~code);
        
        //~~~Call all Delegates
        for ( std::vector<InputReciever*>::iterator it = g_reciever.begin(); it != g_reciever.end(); it++ )
        {
            ((InputReciever*)*it)->KeyReleased((u32)p_code, code);
        }
    }
}

void InputManager::AddInputDelegate(InputReciever* p_del)
{
    g_reciever.push_back(p_del);
}

void InputManager::RemoveInputDelegate(InputReciever* p_del)
{
    auto iter = std::find(g_reciever.begin(), g_reciever.end(), p_del);
    if (iter != g_reciever.end())
    {
        g_reciever.erase(iter);
    }
}

