//
//  Constants.h
//  Dualism
//
//  Created by Charles Alvarez on 8/4/14.
//
//

#ifndef __Dualism__Constants__
#define __Dualism__Constants__

#include <iostream>
#include "cocos2d.h"

typedef signed char     s8;
typedef signed short    s16;
typedef signed int      s32;
typedef unsigned char   u8;
typedef unsigned short  u16;
typedef unsigned int    u32;
typedef unsigned long long us32;

namespace Enum
{
    typedef enum
    {
        KEY_A       = 1<<1,     //~~~Left
        KEY_S       = 1<<2,     //~~~Down
        KEY_D       = 1<<3,     //~~~Right
        KEY_W       = 1<<4,     //~~~Up
        KEY_J       = 1<<5,     //~~~J      = B
        KEY_K       = 1<<6,     //~~~K      = A
        KEY_START   = 1<<7,     //~~~Start  = N
        KEY_SELECT  = 1<<8,     //~~~Select = B
    }ControlKeys;
};


USING_NS_CC;

//~~~Screen Constants 160x144
#define DEBUG_RESO_SCALE    10.0f
//~~~Design Resolution
#define GB_SCREEN_WIDTH     160.0f
#define GB_SCREEN_HEIGHT    144.0f
//~~~Frame Size
#define SCREEN_WIDTH        DEBUG_RESO_SCALE * GB_SCREEN_WIDTH//1024.0f
#define SCREEN_HEIGHT       DEBUG_RESO_SCALE * GB_SCREEN_HEIGHT //768.0f
//~~~Visible Points
#define VISIBLE_SIZE        Director::getInstance()->getVisibleSize()
#define VISIBLE_ORIGIN      Director::getInstance()->getVisibleOrigin()
#define VISIBLE_CENTER      Point(VISIBLE_SIZE.width*0.5f,VISIBLE_SIZE.height*0.5f)
//~~~Camera Pos: Position of the Player on Screen. treat this as Anchor Point on Visible Size
#define CAMERA_CENTER       Point(0.5f,0.5f)
#define ANCHOR_ZERO         Point(0.0f,0.0f)
#define ANCHOR_CENTER       Point(0.5f,0.5f)

//~~~Utils
#define DIRECTOR            Director::getInstance()

#endif /* defined(__Dualism__Constants__) */
