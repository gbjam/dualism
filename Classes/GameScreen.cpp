//
//  GameScreen.cpp
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/2/14.
//
//

#include "GameScreen.h"
#include "GameLayer.h"
#include "Constants.h"

USING_NS_CC;

GameScreen::GameScreen()
{
    
}

GameScreen::~GameScreen()
{
    
}

// on "init" you need to initialize your instance
bool GameScreen::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    //~~~Initialize World
    m_gameLayer     = GameLayer::create();
    this->addChild(m_gameLayer);
    
    //~~~Initialize Player
    m_player        = Player::create();
    this->addChild(m_player);
    
    //~~~Initialize Camera
    MGCamera::Create();
    MGCamera::Instance().Init(m_player, m_gameLayer, CAMERA_CENTER);
    
    return true;
}
