//
//  GameLayer.h
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/2/14.
//
//

#ifndef __Dualism__GameLayer__
#define __Dualism__GameLayer__

#include <iostream>
#include "cocos2d.h"
#include "World.h"

USING_NS_CC;

class GameLayer :
public
    World
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(GameLayer);
};

#endif /* defined(__Dualism__GameLayer__) */
