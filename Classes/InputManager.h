//
//  InputManager.h
//  Dualism
//
//  Created by Charles Alvarez on 8/4/14.
//
//

#ifndef __Dualism__InputManager__
#define __Dualism__InputManager__

#include <iostream>
#include "Constants.h"
#include "cocos2d.h"

USING_NS_CC;

//####TODO: Add Modifying of KeyBoardKeys


class InputReciever
{
public:
    virtual void KeyPressed     ( u32 p_currentOnFlags, Enum::ControlKeys p_keys ) {};
    virtual void KeyReleased    ( u32 p_currentOnFlags, Enum::ControlKeys p_keys ) {};
};

class InputManager
{
private:
    
    u32 m_keyboardFlags;
    
    //~~~Initialization
    InputManager();
    
    //~~~Invoker : Keyboared pressed and released
    void KeyPressed(EventKeyboard::KeyCode p_code, Event* p_event);
    void KeyReleased(EventKeyboard::KeyCode p_code, Event* p_event);
    
public:
    
    //~~~Get Instance
    static InputManager* GetInstance();

    //~~~Add and Remove Delegate
    void RemoveInputDelegate(InputReciever* p_del);
    void AddInputDelegate(InputReciever* p_del);
};

#endif /* defined(__Dualism__InputManager__) */
