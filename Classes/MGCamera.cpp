//
//  Camera.cpp
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/4/14.
//
//

#pragma mark -
#pragma mark Camera Class
#include "MGCamera.h"

#pragma mark Singleton
DEFINE_SINGLETON(MGCamera)

#pragma mark Initializations
MGCamera::MGCamera()
{
    
}

bool MGCamera::Init(
    Player* p_player,
    World* p_world,
    Point p_camPos
) {
    m_status    = EIdle;
    m_player    = p_player;
    m_world     = p_world;
    //~~~ Camera Position
    m_cameraPos = p_camPos;
    return true;
}

#pragma mark Public Functionalities
void MGCamera::Pause()
{
    m_status = EPaused;
}

void MGCamera::Resume()
{
    m_status = ERunning;
}

void MGCamera::Update(float p_dt)
{
    if( m_status == EPaused ) { return; }
    
}

#pragma mark Private Functionalities