//
//  World.cpp
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/4/14.
//
//

#include "World.h"
#include "Player.h"

USING_NS_CC;

Point World::WorldPos(Player* p_player)
{
    Point pos = this->convertToWorldSpace(p_player->getPosition());
    return pos;
}