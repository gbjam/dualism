//
//  Player.cpp
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/4/14.
//
//

#include "Player.h"
#include "Constants.h"

bool Player::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    //~~~Initialize Skin
    m_skin  = Sprite::create("CloseNormal.png");
    m_skin->setAnchorPoint(ANCHOR_CENTER);
    m_skin->setPosition(VISIBLE_CENTER);
    this->addChild(m_skin);
    
    return true;
}