//
//  World.h
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/4/14.
//
//

#ifndef __Dualism__World__
#define __Dualism__World__

#include <iostream>
#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

class Player;
class World :
public
    Layer
{
//~~~Methods
public:
    Point WorldPos(Player* p_player);
};

#endif /* defined(__Dualism__World__) */
