//
//  Player.h
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/4/14.
//
//

#ifndef __Dualism__Player__
#define __Dualism__Player__

#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

class Player :
public
    Layer
{
//~~~Initializations
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
//~~~Properties
private:
    Sprite* m_skin;
    
public:
    // implement the "static create()" method manually
    CREATE_FUNC(Player);
};

#endif /* defined(__Dualism__Player__) */
