//
//  Camera.h
//  Dualism
//
//  Created by Aries Sanchez Sulit on 8/4/14.
//
//

#ifndef __Dualism__MGCamera__
#define __Dualism__MGCamera__

#include <iostream>
#include "cocos2d.h"
#include "MGSingleton.h"

USING_NS_CC;

//~~~TODO: Move this to StateManager. ( Effects, etc classes )
enum State
{
    EIdle,
    EPaused,
    ERunning,
    EInvalid
};

class Player;
class World;
class MGCamera
{
//~~~Singleton
    DECLARE_SINGLETON(MGCamera)
    
//~~~Initializations
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    bool Init(
        Player* p_player,
        World* p_world,
        Point p_camPos
    );
   
//~~~Properties
private:
    Player* m_player;
    World* m_world;
    Point m_cameraPos;
    Point m_prevPos;
    State m_status;
    
//~~~Methods
public:
    void Pause();
    void Resume();
    void Update(float p_dt);
    
};

#endif /* defined(__Dualism__Camera__) */
